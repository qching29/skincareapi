var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors');
var session = require('express-session');
// ---------------------------------

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var croomRouter = require('./routes/croom');
var SliderPageRouter = require('./routes/slider_page');
var ProfessorIdPageRouter = require('./routes/professorId');
//
var itemsRouter = require('./routes/items');
var cartRouter = require('./routes/cart');
var membersRouter = require('./routes/members');
var productsRouter = require('./routes/products');
var deliverRouter = require('./routes/order');
var bvarticleRouter = require('./routes/bvarticle');
var classRouter = require('./routes/classify');

// ---------------------------------


var app = express();
app.use(cors());
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'bv/build')));
app.use(session({
  secret: 'secret',
  resave: true,
  saveUninitialized: true,
}));

// ---------------------------------
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/allcroom', croomRouter);
app.use('/allcategory', SliderPageRouter);
app.use('/professorId', ProfessorIdPageRouter);
//
app.use('/store', itemsRouter);
app.use('/cart', cartRouter);
app.use('/api', membersRouter);
app.use('/api', productsRouter);
app.use('/deliver', deliverRouter);
// app.use('/bv', bvarticleRouter);
app.use('/bv', bvarticleRouter,express.static("public/images/liliimg"));
app.use('/classify', classRouter);

// ---------------------------------

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page 
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
