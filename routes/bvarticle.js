var express = require('express');
var router = express.Router();
var mysql = require("mysql");
const multer = require('multer');

//建立連線
var connection = mysql.createConnection({
  host:'localhost',
  user:'root',
  password:'root',
  database:'mybv',
  port:8889
});
// connection.connect();
connection.connect(function(err) {
  if (err) {
    console.error("error connecting: " + err.stack);
    return;
  }
  console.log("connected as id " + connection.threadId);
});


//設定將上傳的檔案放到reactapp/build/uploads的資料夾下
var uploadFolder = 'public/images/liliimg/art_pic';
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, uploadFolder);    
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname);  
    }
});
var upload = multer({ storage: storage })


router
  .route("/article/articlelist")
  .get(function(req, res) {//讀所有資料
      connection.query("select * from article order by 'id' DESC", function(error,rows){
        if (error) throw error;
        res.json(rows);
      })
  })
  // .post(upload.single('art_pic1'),function(req, res) {//新增資料 ,資料表欄位名稱 
  .post(upload.any(),function(req, res) {
    //console.log(req.files);
  //   for(i=0;i<=art_pic1.length;i++){
  //     // var __user = req.body
  //     // req.body.art_pic1[i]
      req.body.art_pic1=req.files[0].filename;
      req.body.art_pic2=req.files[1].filename;
      req.body.art_pic3=req.files[2].filename;
      connection.query("insert into article set ?", req.body, function(error) {
      if (error) throw error
  //   // //將上傳的檔案名稱取出加到_user物件中
  //   // _user.photo = req.file.filename
  //   console.log(req.files)
  //   // users.push(_user); 
  })
       res.json({message:"新增成功"})
   //}
  }
    )
router
  .route("/article/articlelist/:page")
  .get(function (req, res) {
    //先統計總共幾筆資料
    var query = "select count(*) as TotalCount from article ";
    var totalCount = 0;
    connection.query(query, function (error, row) {
      if (error) throw error;
      totalCount = row[0].TotalCount;

      //讀出分頁資料
      var LimitNum = 12;   //一次讀取10筆資料
      var startNum = 0;    //從第幾筆開始讀
      if (req.params.page) {
        page = parseInt(req.params.page);
        startNum = (page - 1) * LimitNum;
      }
      var query = "select * from article  limit ? OFFSET ? ";
      var params = [LimitNum, startNum];
      query = mysql.format(query, params);
      connection.query(query, function (error, row) {
        if (error) throw error;
        res.json({ "TotalCount": totalCount, "datas": row });
      });
    });
  })



router
  .route("/article/my/:usid")
  .get(function(req, res) {//讀所有資料
      connection.query("select * from article where usid=? ", req.params.usid, function(error,results){
        if (error) throw error;
        res.json(results);
      })
  });
  router
  .route("/article/articlelist/detail/:id")
  .get(function(req, res) {
    connection.query("select * from article where id=?", req.params.id,function(error,results){
      if(error) throw error;
      res.json(results);
    })
  })
  
  .delete(function(req, res) {//刪除資料
    connection.query("delete from article where id=?",req.params.id,function(error,results){
      if(error) throw error;
      res.json({ message: "刪除成功" })
    })
  });

  router
  .route("/article/type1/")
  .get(function(req, res) {//讀所有資料
      connection.query("select * from article where type_num=1 ", req.params.type_num, function(error,results){
        if (error) throw error;
        res.json(results);
      })
  });
  router
  .route("/article/type2/")
  .get(function(req, res) {//讀所有資料
      connection.query("select * from article where type_num=2 ", req.params.type_num, function(error,results){
        if (error) throw error;
        res.json(results);
      })
  });
  router
  .route("/article/type3/")
  .get(function(req, res) {//讀所有資料
      connection.query("select * from article where type_num=3 ", req.params.type_num, function(error,results){
        if (error) throw error;
        res.json(results);
      })
  });
  router
  .route("/article/type4/")
  .get(function(req, res) {//讀所有資料
      connection.query("select * from article where type_num=4 ", req.params.type_num, function(error,results){
        if (error) throw error;
        res.json(results);
      })
  });
  router
  .route("/article/type/")
  .get(function(req, res) {//讀所有資料
      connection.query("select * from article where type_num=5 ", req.params.type_num, function(error,results){
        if (error) throw error;
        res.json(results);
      })
  });

  router
  .route("/article/articlelist/type/:id")
  .get(function(req, res) {
    connection.query("select * from article where id=?", req.params.id,function(error,results){
      if(error) throw error;
      res.json(results);
    })
  })



router
  .route("/article/articlelist/:id")
  .get(function(req, res) {
    connection.query("select * from article where id=?", req.params.id,function(error,results){
      if(error) throw error;
      res.json(results);
    });
  
  }) 
  .put(function(req, res) {//修改資料
       var _member = req.body;  
       var id = req.params.id;
       connection.query("update article set ? where id=?",[_member, id],function(error){
          if(error) throw error;
          res.json({ message: "修改成功" });
       })
  }) 
  .delete(function(req, res) {//刪除資料
    connection.query("delete from article where id=?",req.params.id,function(error){
      if(error) throw error;
      res.json({ message: "刪除成功" });
    })
  }); 




module.exports = router;
