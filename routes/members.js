var express = require("express");
var router = express.Router();
var mysql = require("mysql");

//建立連線
var connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'root',
  database: 'mybv',
  port:8889
});
connection.connect();
var moment = require('moment');

// http://localhost:3000/api/members
router
  .route("/members")
  .get(function (req, res) {
    //GET http://localhost:3000/xxx/members
    //  res.send("get all product");
    connection.query("select * from members", function (error, results) {
      if (error) throw error;

      results.forEach((item)=>{
        
        item.created_at=moment(item.created_at).format('YYYY/MM/DD,HH:mm:ss')
      })
    //   for (i=0; i <results.length; i++){
    //     results[i].created_at = moment(results[i].created_at).format('YYYY/MM/DD,HH:mm:ss');
    // }
      res.json(results);
    });
  })
  // .post(function (req, res) {
  //   //POST http://localhost:3000/xxx/members
  //   //讀取瀏覽器傳送過來的資料  req.body
  //   // res.json({ message: "新增成功123" });
  //   // console.log("234")
  //   // connection.query("insert into members set ?", req.body, function (error) {
  //   //   if (error) throw error;})
  //   connection.query("select * from `members` where email=?", req.body.email, function (error,rows) {
  //     // console(rows.length)
  //     if (rows.length >= 1) {
  //       res.json({ message: "mail used" });
  //     } else {
  //       connection.query("insert into `members` set ?", req.body.email, function (error) {
  //         if (error) throw error;
  //         res.json({ message: "新增成功" });
  //       });
  //     }
  //   });

  // });
  // ============================================
  .post(function (req, res) {  //新增資料
    var _user = req.body.email;
    // connection.query("select * from members where email = ? ", _user, function (error, rows) {
    //   console.log(rows);
    //   if (rows[0] !== "") {
    //     res.json({ message: "註冊失敗, 已有重複的E-mail" });
    //   } else {
    //     connection.query("insert into members set ?", _user, function (error, result) {
    //       if (error) throw error;
    //       res.json({ message: "註冊成功" });
    //     })
    //   }
    // })
    connection.query("insert into members set ?", req.body, function (error, rows) {
      if (error) throw error;
      // res.json({ message: "註冊成功" ,rows});
      // res.redirect('http://localhost:3003/member')
    })
// =================test
connection.query("select * from members where email=?", [req.body.email], function (error, rows) {
  if (error) throw error;
  rows[0].created_at=moment(rows[0].created_at).format('YYYY/MM/DD,HH:mm:ss');
  res.json({ message: "註冊成功" ,rows});
});

// ============

  });

// http://localhost:3000/api/members/1
router
  .route("/members/:id")
  .get(function (req, res) {
    //GET http://localhost:3000/xxx/members/2
    //讀取:id參數的值 req.params.id
    // res.json("get product id " + req.params.id);
    // var a = JSON.parse(localStorage.getItem("member"));

    connection.query(
      // "select * from members where id=?", a[0].id, function (error, results) {

      "select * from members where id=?", req.params.id, function (error, results) {
        if (error) throw error;
        res.json(results);
      }
    );
  })
  .put(function (req, res) {
    //PUT http://localhost:3000/xxx/members/2
    //讀取瀏覽器傳送過來的資料  req.body
    //修改資料
    //res.send("修改 " + req.params.id + " 資料");
    connection.query("update members set ? where id=?", [req.body, req.params.id], function (error) {
      if (error) throw error;
      res.json({ message: "修改成功" });
    });
  })
  .delete(function (req, res) {
    //DELETE http://localhost:3000/xxx/members/2
    //刪除資料
    connection.query(
      "delete from members where id=?", req.params.id, function (error, results) {
        if (error) throw error;
        res.json({ message: "刪除成功" });
      }
    );
  });
router
  .route("/members_login")

  //GET http://localhost:3000/xxx/members/2
  //讀取:id參數的值 req.params.id
  //res.send("get product id " + req.params.id);
  .get(function (req, res) {
    //GET http://localhost:3000/xxx/members
    //  res.send("get all product");
    connection.query("select * from members where email=?", [req.body.email], function (error, results) {
      if (error) throw error;
      res.json(results);
    });
  })
  .post(function (req, res) {
    // res.setHeader("Access-Control-Allow-Origin", "http://localhost:3001");
    // res.setHeader("Access-Control-Allow-Credentials", "true");
    //GET http://localhost:3000/xxx/members/2
    //讀取:id參數的值 req.params.id
    //res.send("get product id " + req.params.id);
    // res.send(req.params.email);
    console.log('req', req);
    //console.log('res', res);
    console.log(req.body.email);
    connection.query(
      "select * from members where email=?", [req.body.email],
      function (error, rows) {
        if (error) throw error;
        // if (rows[0] == '') {
        //   res.json({ message: "帳號不存在" });
        // }
        if (rows == '') {
          res.json({ message: "帳號不存在" });
        } else if (rows[0].password != req.body.password) {
          res.json({ message: "密碼錯誤" });
        }
        else {
          // res.json({ message: "登入成功" });
          // console.log(rows);
          rows[0].created_at=moment(rows[0].created_at).format('YYYY/MM/DD,HH:mm:ss');
          // res.json(rows[0].created_at);
          // res.json(rows);
          res.json({ message: "登入成功",rows});


        }
        // else {
        // res.json(rows[0].password);
        // }
        // res.json({ message: "登入成功" });
      }
    );
  })
module.exports = router;
