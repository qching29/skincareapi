var express = require('express');
var router = express.Router();

var mysql = require('mysql');

var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'mybv',
    port:8889
  });
connection.connect();

router.route('/items')
    .get(function(req,res){
       connection.query("SELECT * From product",function(error,results){
            if(error) throw error;
            res.json(results);
       })
    })
    .post(function(req,res){
        connection.query("INSERT INTO product SET ?",req.body,function(error){
            if(error) throw error;
            res.json({message:"新增成功"})
       })
    })

router.route('/items/:id')
    .get(function(req,res){
        connection.query("SELECT * From product where product_id=?",req.params.id,function(error,results){
            if(error) throw error;
            res.json(results);
        })
    })
    .put(function(req,res){
        connection.query("UPDATE product set ? where product_id=?",[req.body, req.params.id], function(error,results){
            if(error) throw error;
            res.json(results);
        })

    })
    .delete(function(req,res){
        connection.query("DELETE From product where product_id=?",req.params.id,function(error,results){
            if(error) throw error;
            res.json(results);
        })
        res.send("刪除資料");
    })

module.exports = router;