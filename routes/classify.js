var express = require('express');
var router = express.Router();
var session = require('express-session');

var mysql = require('mysql');

var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'mybv',
    port:8889
  });
connection.connect(function (err) {
    if (err) {
        console.error("error connecting: " + err.stack);
        return;
    }
    console.log("connected as id" + connection.threadID)
});

router.route('/product')
    .get(function (req, res) {
        connection.query("SELECT * From product", function (error, results) {
            if (error) throw error;
            res.json(results);
        })
    })

router.route("/product/:category")
    .get(function (req, res) {
        connection.query(`select product.productimages,
                     product.productname,
                     product.productprice,
                     Product.product_id
                     from product
                     join category on product.type_id = category.type_id
                     WHERE product.type_id=?`, req.params.category, function (error, results) {
                if (error) throw error;
                res.json(results);
                res.end();
            })
    })

module.exports = router;