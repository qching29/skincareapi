var express = require('express');
var router = express.Router();
var mysql = require('mysql');

var connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'root',
  database: 'mybv',
  port:8889
});
connection.connect(function (err) {
  if (err) {
    console.error("error connecting: " + err.stack);
    return;
  }

});
//Link課程總攬的API
// http://localhost:3000/allcategory/slider_page
router.route('/slider_page')
  .get(function (req, res) {
    // res.send('get...all')
    connection.query("select c_room.idx, c_room.cs_id,(select user_img from professor where user_id = c_room.user_id) as 'img', c_room.user_id, c_room.c_category , c_room.c_title, c_room.c_area, c_room.c_people, c_room.amount, c_room.c_total_time, c_room.c_img1,c_room.start,course_category.link_name,course_category.img_url,(select  (c_room.c_people-SUM(buy_purchase))as 'peop' from c_order where c_id=c_room.cs_id ) as 'total_pop'  from c_room right join course_category on c_room.c_category = course_category.idx order by c_room.idx DESC", function (error, results) {
      if (error) throw error;
      res.json(results);
    });
  })

// http://localhost:3000/allcategory/slider_page/1/1
// router.route('/slider_page/:page/:class')
//     .get(function (req, res) {
//         //先統計總共幾筆資料
//         var query = "select count(*) as TotalCount from c_room WHERE c_category=" + req.params.class;
//         var totalCount = 0;
//         connection.query(query, function (error, row) {
//             if (error) throw error;
//             totalCount = row[0].TotalCount;
//             console.log(row)
//             //讀出分頁資料
//             var LimitNum = 8;   //一次讀取10筆資料
//             var startNum = 0;    //從第幾筆開始讀
//             if (req.params.page) {
//                 page = parseInt(req.params.page);
//                 startNum = (page - 1) * LimitNum;
//             }
//             var query = "select c_room.idx, c_room.cs_id,(select user_img from professor where user_id = c_room.user_id) as 'img', c_room.user_id, c_room.c_category , c_room.c_title, c_room.c_area, c_room.c_people, c_room.amount, c_room.c_total_time, c_room.c_img1,c_room.start,course_category.link_name,course_category.img_url, from c_room right join course_category on c_room.c_category = course_category.idx WHERE c_room.c_category= " + req.params.class;;
//             var params = [LimitNum, startNum];
//             query = mysql.format(query, params);
//             connection.query(query, function (error, row) {
//                 if (error) throw error;
//                 res.json({ "TotalCount": totalCount, "datas": row });
//             });
//         });
//     })


//課程類別功能的API
router
  .route("/slider_page/:category")
  .get(function (req, res) {
    connection.query("select c_room.idx, c_room.cs_id,(select user_img from professor where user_id = c_room.user_id) as 'img', c_room.user_id, c_room.c_category , c_room.c_title, c_room.c_area, c_room.c_people, c_room.amount, c_room.c_total_time, c_room.c_img1,c_room.start,course_category.link_name,course_category.img_url from c_room right join course_category on c_room.c_category = course_category.idx WHERE c_room.c_category=? order by c_room.idx DESC",req.params.category, function (error, rows) {
      if (error) throw error;
      res.json(rows);
    })
  })
//未完成
  // router
  // .route("/slider_page/:id/:category/:start")
  // .get(function (req, res) {
  //   var id = req.params.id;
  //   var category = req.params.category;
  //   var start = req.params.start;
  //   connection.query("select c_room.idx, c_room.cs_id,(select user_img from professor where user_id = c_room.user_id) as 'img', c_room.user_id, c_room.c_category , c_room.c_title, c_room.c_area, c_room.c_people, c_room.amount, c_room.c_total_time, c_room.c_img1,c_room.start,course_category.link_name,course_category.img_url from c_room right join course_category on c_room.c_category = course_category.idx WHERE c_room.user_id=? and  c_room.c_category=? and c_room.start=?",[id,category,start], function (error, rows) {
  //     if (error) throw error;
  //     res.json(rows);
  //   })
  // })
// http://localhost:3000/allcategory/slider_pag1
// router.route('/slider_page/')
//   .get(function (req, res) {
//     //先統計總共幾筆資料
//     connection.query("select c_room.idx, c_room.cs_id,(select user_img from professor where user_id = c_room.user_id) as 'img', c_room.user_id, c_room.c_category , c_room.c_title, c_room.c_area, c_room.c_people, c_room.amount, c_room.c_total_time, c_room.c_img1,c_room.start,course_category.link_name,course_category.img_url from c_room right join course_category on c_room.c_category = course_category.idx WHERE c_room.c_category=3", function (error, results) {
//       if (error) throw error;
//       res.json(results);
//     });
//   })
//課程詳細頁+id
// http://localhost:3000/allcategory/c_room_product
router.route('/c_room_product/:id')
  .get(function (req, res) {
    // res.send('get...all')
    connection.query("select c_room.idx, c_room.cs_id,(select productimages from product where product_id = c_room.product_1) as 'img',(select description from product where product_id = c_room.product_1) as 'text',(select product_id from product where product_id = c_room.product_1) as 'prodid',(select productname from product where product_id = c_room.product_1) as 'name', c_room.user_id, c_room.c_category , c_room.c_title, c_room.c_area, c_room.c_people, c_room.amount, c_room.c_total_time, c_room.c_img1,c_room.start,professor.user_img,professor.teacher_name,professor.my_inform from c_room right join professor on c_room.user_id = professor.user_id where c_room.cs_id=?", req.params.id, function (error, results) {
      if (error) throw error;
      res.json(results);
    });
  })

//講師已課程start/:使用者/:狀態
router.route('/mystart/:id/:start')
.get(function (req, res) {
  var id = req.params.id; 
  var start = req.params.start;
  connection.query("select c_room.idx, c_room.cs_id,(select link_name from course_category where course_category = c_room.c_category) as 'link_name',(select img_url from course_category where course_category = c_room.c_category) as 'img_url',(select start_name from start where start_nb = c_room.start) as 'start_name',c_room.user_id, c_room.c_category , c_room.c_title, c_room.c_area, c_room.c_people, c_room.amount, c_room.c_total_time, c_room.c_img1,c_room.start,professor.user_img,professor.teacher_name,professor.my_inform from c_room right join professor on c_room.user_id = professor.user_id where c_room.user_id=? and c_room.start=?",[id,start], function (error, results) {
    if (error) throw error;
    res.json(results);
  });
})
//講師參加課程start/:使用者/:狀態
router.route('/myjoinstart/:id')
.get(function (req, res) {
  var id = req.params.id; 
  var start = req.params.start;
//講師已課程start/:使用者/:狀態
  connection.query("select c_order.c_id,c_order.user_id, (select link_name from course_category where course_category = c_room.c_category) as 'link_name', (select img_url from course_category where course_category = c_room.c_category) as 'img_url', (select start_name from start where start_nb = c_room.start) as 'start_name', (select user_img from professor where user_id = c_room.user_id) as 'teacher_img', c_room.user_id,c_room.cs_id, c_room.c_category , c_room.c_title, c_room.c_area, c_room.c_people, c_room.amount, c_room.c_total_time, c_room.c_img1,c_room.start, c_order.user_id from c_order INNER join c_room on c_order.c_id = c_room.cs_id where c_order.user_id=?",req.params.id, function (error, results) {
    if (error) throw error;
    res.json(results);
  });
})

//講師詳已開課程+已參加課程統計總數
router.route('/order/:id/:start')
.get(function (req, res) {
  var id = req.params.id; 
  var start = req.params.start;
  connection.query("SELECT COUNT(user_id) AS `upclass`, (SELECT COUNT(user_id) user_id from c_order WHERE c_order.user_id=c_room.user_id) AS `c_order` FROM `c_room` WHERE user_id=? and start=?",[id,start], function (error, results) {
    if (error) throw error;
    res.json(results);
  });
})
//total統計剩餘人數
// http://localhost:3000/allcategory/category_page
router.route('/category_page')
    .get(function (req, res) {
        // res.send('get...all')
        connection.query("select c_room.idx, c_room.cs_id, (select user_img from professor where user_id = c_room.user_id) as 'img', c_room.user_id, c_room.c_category , c_room.c_title, c_room.c_area, c_room.c_people, c_room.amount, c_room.c_total_time, c_room.c_img1,c_room.start,course_category.link_name,course_category.img_url,(select  (c_room.c_people-SUM(buy_purchase))as 'peop' from c_order where c_id=c_room.cs_id ) as 'total_pop' from c_room right join course_category on c_room.c_category = course_category.idx", function (error, results) {
            if (error) throw error;
            res.json(results);
        });
    })
module.exports = router;