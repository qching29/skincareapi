var express = require('express');
var router = express.Router();
var mysql = require('mysql');

var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'mybv',
    port:8889
  });
connection.connect(function (err) {
    if (err) {
        console.error("error connecting: " + err.stack);
        return;
    }
});
//講師首頁資訊API+講師Link
// http://localhost:3000/professorId/professor
router.route('/professor')
    .get(function (req, res) {
        // res.send('get...all')
        connection.query("SELECT professor.user_id, professor.teacher_name, professor.user_img, professor.link_url1, professor.link_class1, professor.link_url2, professor.link_class2, professor.start_count, professor.attend_count, professor.my_inform, professor.c_category, professor.l_img1, professor.l_img2, professor.experience, professor.create_dTime, professor.update_dTime, professor.start ,course_category.link_name from professor right join course_category on professor.c_category = course_category.idx", function (error, results) {
            if (error) throw error;
            res.json(results);
        });
    })
    // .post(function (req, res) {
    //     var __user = req.body
    //     connection.query("insert into professor set ?", __user, function (error) {
    //         if (error) throw error;
    //         res.json({ message: "新增成功" });
    //     });
    // });
//講師詳細頁API
router.route('/professor/:id')
    .get(function (req, res) {
        connection.query("SELECT professor.user_id, professor.teacher_name, professor.user_img, professor.link_url1, professor.link_class1, professor.link_url2, professor.link_class2, professor.start_count, professor.attend_count, professor.my_inform, professor.c_category, professor.l_img1, professor.l_img2, professor.experience, professor.create_dTime, professor.update_dTime, professor.start ,course_category.link_name from professor right join course_category on professor.c_category = course_category.idx where user_id=?", req.params.id, function (error, results) {
            if (error) throw error;
            res.json(results);
        });
    })
    // .put(function (req, res) {
    //     connection.query("update professor set ? where idx=?", [req.body, req.params.id], function (error) {
    //         if (error) throw error;
    //         res.json({ message: "修改成功" });
    //     });
    // })
    // .delete(function (req, res) {
    //     connection.query("delete from professor where idx=?", req.params.id, function (error, results) {
    //         if (error) throw error;
    //         res.json({ message: "刪除成功" });
    //     });
    // });
//參加課程start狀態API
router.route('/user_start/:id')
    .get(function (req, res) {
        // res.send('get...all')
        connection.query("select c_room.idx, c_room.cs_id,(select link_name from course_category where course_category = c_room.c_category) as 'link_name',(select img_url from course_category where course_category = c_room.c_category) as 'img_url',(select start_name from start where start_nb = c_room.start) as 'start_name',c_room.user_id,c_room.c_opdate, c_room.c_category , c_room.c_title, c_room.c_area, c_room.c_people, c_room.amount, c_room.c_total_time, c_room.c_img1,c_room.start,professor.user_img,professor.teacher_name,professor.my_inform from c_room right join professor on c_room.user_id = professor.user_id where c_room.user_id=? ", req.params.id,function (error, results) {
            if (error) throw error;
            res.json(results);
        });
    })
module.exports = router;