var express = require('express');
var router = express.Router();
var session = require('express-session');
var moment = require('moment');
var mysql = require('mysql');

var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'mybv',
    port:8889
  });
connection.connect(function (err) {
    if (err) {
        console.error("error connecting: " + err.stack);
        return;
    }
    console.log("connected as id" + connection.threadID)
});

router.route('/order')
    // .get(function (req, res) {
    //     console.log("get in")
    //     connection.query(`SELECT order01.orderid From order01 where userid`,function(error, results) {
    //         if (error) throw error;
    //         res.json(results);
    //     })
    // })
    .get(function (req, res) {
            console.log("get in")
            connection.query(`select order01.orderid,
            order01.pay,
            prod.productname,
            prod.productprice,
            detail.qty,
            detail.qty * prod.productprice as total,
            order01.order_date
     from order01
     join orderdetail as detail on order01.orderid = detail.orderid
     join product as prod on detail.product_id = prod.product_id`,function(error, results) {
         results.forEach(el => {
             el.order_date = moment(el.order_date).format('YYYY-MM-DD HH:mm:ss');
         });

                if (error) throw error;
                res.json(results);
            })
        })
    .post(function (req, res) {
        connection.query("INSERT INTO order01 SET ?", req.body, function (error) {
            if (error) throw error;
            res.json({ message: "新增成功" })
            res.end();
        })
    })

    .put(function (req, res) {
        console.log('get in');
        console.log("customername:" + req.body.customername);
        console.log("customermail:" + req.body.customermail);
        console.log("customermobile" + req.body.customermobile);
        console.log("receivename" + req.body.receivename);
        console.log("receivemobile" + req.body.receivemobile);
        console.log("receiveaddress" + req.body.receiveaddress);
        connection.query(`UPDATE order01 set customername=${"'" + req.body.customername + "'"},
                                            customermail=${"'" + req.body.customermail + "'"},
                                            customermobile=${req.body.customermobile},
                                            receivename=${"'" + req.body.receivename + "'"},
                                            receivemobile=${req.body.receivemobile},
                                            receiveaddress=${"'" +req.body.receiveaddress + "'"}
                                            where userid=${req.body.userid} and orderid=${req.body.orderid}`, req.body, function (error) {
                if (error) throw error;
                // res.json({ message: "新增成功" });
                res.end();
            })
    })

// router.route('/order/orderdetail')
//    .post(function(req,res){
//         connection.query("INSERT INTO orderdetail SET ?",req.body,function(error) {
//             if(error) throw error;
//             res.json({message:'新增成功'})
//             res.end();
//         })
//    })

module.exports = router;