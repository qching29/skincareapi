var express = require('express');
var router = express.Router();
var session = require('express-session');

var mysql = require('mysql');

var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'mybv',
    port:8889
  });
connection.connect(function (err) {
    if (err) {
        console.error("error connecting: " + err.stack);
        return;
    }
    console.log("connected as id" + connection.threadID)
});

router.route('/Addcart')
    .get(function (req, res) {
        // require.session.product_id = 'product_id';
        // require.session.qty = 'qty';
        connection.query(`select prod.product_id,
                                 prod.productimages,
                                 prod.productname, 
                                 sum(list.qty) as amount,
                                 prod.productprice,
                                 prod.productprice * sum(list.qty) as total 
                          from shopping_list as list 
                          join product as prod on list.product_id = prod.product_id
                          group by list.product_id`, function (error, results) {
                if (error) throw error;
                res.json(results);
            })
    })

    .post(function (req, res) {
        connection.query("INSERT INTO shopping_list SET ?", req.body, function (error) {
            if (error) throw error;
            res.json({ message: "新增成功" })
            res.end();
        })
    })

    .put(function (req, res) {
        connection.query(`UPDATE shopping_list 
                            set qty = ${req.body.qty}
                             where userid = ${req.body.userid}
                             and product_id = ${req.body.product_id}`, req.body, function (error) {
                if (error) throw error;
                res.json({ message: "新增成功" });
                res.end();
            })
    })

router.route('/Addcart/:id')
    .delete(function (req, res) {
        connection.query(`DELETE From shopping_list where product_id = ${req.params.id}`, function (error) {
            if (error) throw error;
        })
        res.send("刪除資料");
        res.end();
    })

router.route('/Addcart/orderdetail')
    .post(function (req, res) {
        
        connection.query("INSERT INTO orderdetail SET ?", req.body, function (error) {
            if (error) throw error;
            res.json({ message: "新增成功" })
            res.end();
        })
    })
// .put(function (req, res) {
//     connection.query(`UPDATE orderdetail 
//                         set qty = ${req.body.qty}
//                          where orderid = ${req.body.orderid}
//                          and product_id = ${req.body.product_id}`, req.body, function (error) {
//             if (error) throw error;
//             res.json({ message: "新增成功" });
//             res.end();
//         })
// })

// router.route('/Addcart/orderdetail/:id')
//     .delete(function (req, res) {
//         connection.query(`DELETE From orderdetail where product_id = ${req.params.id}`, function (error) {
//             if (error) throw error;
//         })
//         res.send("刪除資料");
//         res.end();
//     })

module.exports = router;