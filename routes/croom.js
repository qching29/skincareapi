var express = require('express');
var router = express.Router();
var mysql = require('mysql');
const multer = require('multer');

var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'mybv',
    port:8889
  });
connection.connect(function (err) {
    if (err) {
        console.error("error connecting: " + err.stack);
        return;
    }
});
//課程內容狀態修改
// http://localhost:3000/allcroom/croom
router.route('/croom')
    .get(function (req, res) {
        // res.send('get...all')
        connection.query("SELECT idx, cs_id, user_id, c_category, c_title, c_address, c_area, c_phone, c_people, amount, c_total_time, c_opdate, c_detail, c_img1, c_img2, product_1, create_dTime, update_dTime, start, note,(select (c_room.c_people-SUM(buy_purchase))as 'peop' from c_order where c_id=c_room.cs_id ) as 'total_pop' FROM c_room ", function (error, results) {
            if (error) throw error;
            res.json(results);
        });
    })
    .post(function (req, res) {
        var __user = req.body
        connection.query("insert into c_room set ?", __user, function (error) {
            if (error) throw error;
            res.json({ message: "新增成功" });
        });
    })
    .put(function (req, res) {
        connection.query("update c_room set start=? where user_id=? and cs_id=?", [req.body.start, req.body.userid,req.body.cs_id], function (error) {
            if (error) throw error;
            res.json({ message: "修改成功" });
        });
    })
    .delete(function (req, res) {
        connection.query("delete from c_room where idx=?", req.params.id, function (error, results) {
            if (error) throw error;
            res.json({ message: "刪除成功" });
        });
    });
// http://localhost:3000/allcroom/croom
    //課程內容+id
router.route('/croom/:id')
    .get(function (req, res) {
        console.log('res',req.params)
        connection.query("SELECT idx, cs_id, user_id, c_category, c_title, c_address, c_area, c_phone, c_people, amount, c_total_time, c_opdate, c_detail, c_img1, c_img2, product_1, create_dTime, update_dTime, start, note,(select (c_room.c_people-SUM(buy_purchase))as 'peop' from c_order where c_id=c_room.cs_id ) as 'total_pop' FROM c_room where cs_id=?", req.params.id, function (error, results) {
            if (error) throw error;
            res.json(results);
        });
    })



//新增課程內容
// http://localhost:3000/allcroom/newcroom
//設定將上傳的檔案放到reactapp/build/uploads的資料夾下
var uploadFolder = 'bv/public/images';
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, uploadFolder);    
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname);  
    }
});
var upload = multer({ storage: storage })
router.route('/newcroom')
    .get(function (req, res) {//讀所有資料
        // res.send('get...all')
        connection.query("SELECT * from c_room order by idx desc ", function (error, rows) {
            if (error) throw error;
            res.json(rows);
        });
    })
    .post(upload.single('c_img1'),function(req, res) {//新增資料 ,資料表欄位名稱 
        console.log(req.body.c_img1)
        var _user = req.body;
        // console.log(file)
        //將上傳的檔案名稱取出加到_user物件中
        _user.c_img1 = req.file.filename;
        
        connection.query("insert into c_room set ?", _user, function (error) {
            if (error) throw error;
            res.json({ message: "新增上傳" });
        });
    })    
    

module.exports = router;